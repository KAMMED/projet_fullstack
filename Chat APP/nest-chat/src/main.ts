import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(cookieParser());
  app.enableCors({
    origin: ["http://localhost:3000", "http://localhost:8080", "http://localhost:4200", "http://192.168.1.102:4200","http://192.168.242.122:4200","http://192.168.1.102:3000"],
    credentials: true
  })
  await app.listen(8000);
}
bootstrap();
