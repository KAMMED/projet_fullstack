import { Injectable } from '@nestjs/common';
import * as Pusher from "pusher";

@Injectable()
export class PusherService {
    pusher: Pusher;

    constructor() {
      this.pusher = new Pusher({
        appId: "1778816",
        key: "d6e50b21aa7afae770df",
        secret: "8a2767c8a58cf7fb5748",
        cluster: "eu",
        useTLS: true
      });
    }
  
    async trigger(channel: string, event: string, data: any) {
      await this.pusher.trigger(channel, event, data);
    }
  }